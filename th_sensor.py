import board
import adafruit_am2320


class Sensor():
    def initialisation_sensor(self):
        """ things to have the value from the sensor"""
        self.i2c = board.I2C()
        self.sensor = adafruit_am2320.AM2320(self.i2c)

    def values(self):
        """ return the values of the tenprature and relative hunidity once"""
        self.initialisation_sensor()
        self.temperature = self.sensor.temperature
        self.humidity = self.sensor.relative_humidity
        return self.temperature, self.humidity
