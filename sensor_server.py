import socket
import threading
import time
import sys

from th_sensor import Sensor


def intialisation_connexion(clients, usernames, client, address):
    print("Connected with {}".format(str(address)))
    # Store username
    username = client.recv(1024).decode('utf-8')
    usernames.append(username)
    clients.append(client)
    print("Username is {}".format(username))


# # Sending Messages To All Connected Clients
def broadcast(message, clients):
    for client in clients:
        client.send(message)


# Handling Messages From Clients
def handle(clients, client, usernames, address):
    while True:
        time.sleep(2)
        try:
            # Broadcasting Messages
            sensor = Sensor()
            temperature, humidity = sensor.values()
            message = (str(temperature) + " " + str(humidity))
            msg = message.encode('utf-8')
            broadcast(msg, clients)
            time.sleep(600)
        except:
            # Removing And Closing Clients
            index = clients.index(client)
            clients.remove(client)
            client.close()
            username = usernames[index]
            msg = '{} not connected'.format(username).encode('utf-8')
            broadcast(msg, clients)
            usernames.remove(username)
            break


# Receiving / Listening Function
def connexion(server, clients, usernames):
    while True:
        # Accept Connection
        client, address = server.accept()
        intialisation_connexion(clients, usernames, client, address)
        client.send('CONNECTED'.encode('utf-8'))
        thread = threading.Thread(target=handle, args=(clients, client, usernames, address,))
        thread.start()


def main():
    # Connection Data
    host = 'localhost'
    port = 12340

    # Starting Server
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        server.bind((host, port))
    except socket.error as msg:
        print(msg)
        server.close()
        sys.exit(1)
    server.listen()

    # Lists For Clients and Their usernames
    clients = []
    usernames = []

    print('The sensor server is running ...')
    connexion(server, clients, usernames)


if __name__ == '__main__':
    main()
