import json
import re
from matplotlib import pyplot as plt
import mplcyberpunk
import argparse


def open_file(filename):
    fileDir = os.path.dirname(os.path.abspath(__file__))
    directory = fileDir + '/datas_files'
    if not os.path.isdir(directory):
        os.mkdir(directory)
    PATH = directory + '/' + filename
    with open(PATH) as json_file:
        data = json.load(json_file)
        return data


def time_array(data):
    time = []
    for i in range(len(data)):
        cut_date = re.split(r"[.\s]", data[i])
        time.append(cut_date[1])
    return time


def main():
    plt.style.use("cyberpunk")
    # command line option to plot the graph
    parser = argparse.ArgumentParser(
        description='plot the temperature and hunidity from the sensor file saved')
    parser.add_argument('-f', '--filename', required=True, help='filename to plot')
    args = vars(parser.parse_args())

    filename = args["filename"]

    filename = args["filename"]
    data = open_file(filename)

    date = data[0]
    time = time_array(date)
    temperature = data[1]
    humidity = data[2]

    fig = plt.figure(constrained_layout=False)
    fig.suptitle('Temperature & Humidity vs Time', fontsize=15)
    ax1 = fig.add_subplot(211)
    ax1.plot(time, temperature)
    ax1.set_ylim(bottom=0, top=(max(temperature) + 5))
    ax1.set_ylabel("Temperature ($^\circ$C)", fontsize=12)
    ax1.xaxis.set_tick_params(which='both', rotation=45)
    mplcyberpunk.add_glow_effects()

    ax2 = fig.add_subplot(212)
    ax2.plot(time, humidity, 'yellow')
    ax2.set_ylim(bottom=0, top=(max(humidity) + 10))
    ax2.set_ylabel("Humidity (%)", fontsize=12)
    ax2.set_xlabel("Time", fontsize=14)
    ax2.xaxis.set_tick_params(which='both', rotation=45)
    mplcyberpunk.add_glow_effects()
    fig.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()
