from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QApplication
import sys
import socket
import os
import datetime
import json
from pynotifier import Notification
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import datetime as dt
from matplotlib import pyplot as plt
import mplcyberpunk

import sensor_gui
import temp_graph
import hum_graph


class HumGraph(QtWidgets.QMainWindow, hum_graph.Ui_MainWindow):
    def __init__(self, humidity, time, parent=None):
        super(HumGraph, self).__init__(parent=parent)
        self.setupUi(self)
        self.parent = parent
        self.humidity = humidity
        self.time = time
        plt.style.use("cyberpunk")
        self.fig = Figure(tight_layout=True)
        self.axes = self.fig.add_subplot(111)
        self.canvas = FigureCanvas(self.fig)
        self.hum_gridLayout.addWidget(self.canvas)
        self.timer = QtCore.QTimer()
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.update_plot)
        self.timer.start()

    def update_plot(self):
        self.axes.cla()
        self.axes.set_title("Humidity vs Time", fontsize=15)
        self.axes.plot(self.time, self.humidity, "yellow")
        self.axes.set_ylabel("Humidity ()%)", fontsize=12)
        self.axes.set_xlabel("Time", fontsize=14)
        self.axes.xaxis.set_tick_params(which='both', rotation=45)
        mplcyberpunk.add_glow_effects(self.axes)
        self.axes.set_ylim(bottom=0, top=(max(self.humidity) + 10))
        self.canvas.draw()


class TempGraph(QtWidgets.QMainWindow, temp_graph.Ui_MainWindow):
    def __init__(self, temperature, time, parent=None):
        super(TempGraph, self).__init__(parent=parent)
        self.setupUi(self)
        self.parent = parent
        self.temperature = temperature
        self.time = time
        plt.style.use("cyberpunk")
        self.fig = Figure(constrained_layout=False)
        self.axes = self.fig.add_subplot(111)
        self.canvas = FigureCanvas(self.fig)
        self.temp_gridLayout.addWidget(self.canvas)
        self.timer = QtCore.QTimer()
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.update_plot)
        self.timer.start()

    def update_plot(self):
        self.axes.cla()
        self.axes.set_title("Temperature vs Time", fontsize=15)
        self.axes.plot(self.time, self.temperature)
        self.axes.set_ylabel("Temperature ($^\circ$C)", fontsize=12)
        self.axes.set_xlabel("Time", fontsize=14)
        self.axes.xaxis.set_tick_params(which='both', rotation=45)
        self.axes.set_ylim(bottom=0, top=(max(self.temperature) + 5))
        mplcyberpunk.add_glow_effects(self.axes)
        self.fig.tight_layout()
        self.canvas.draw()


# Thread receive messages from the server and then send to the textBrowser
class Receive_thread(QtCore.QThread):
    message_signal = QtCore.pyqtSignal('PyQt_PyObject')
    state_signal = QtCore.pyqtSignal('PyQt_PyObject')

    def __init__(self, client, parent=None):
        super(Receive_thread, self).__init__(parent)
        self.client = client

    def run(self):
        while True:
            try:
                # Receive Message From Server
                message = self.client.recv(1024).decode('utf-8')
                if message == 'CONNECTED':
                    state = 'Connected'
                    self.state_signal.emit(state)
                else:
                    self.message_signal.emit(message)
            except ConnectionAbortedError:
                self.message_signal.emit("Can't connect to the sensor server")
            except Exception as e:
                # Close Connection When Error
                print(e)
                self.message_signal.emit(e)
                self.client.close()


class WeatherApp(QtWidgets.QMainWindow, home_weather_gui.Ui_MainWindow):
    def __init__(self):
        super(WeatherApp, self).__init__()
        self.setupUi(self)
        self.connexion_sever()
        self.identification()
        self.time_array = []
        self.temp_array = []
        self.hum_array = []
        self.data = [self.time_array, self.temp_array, self.hum_array]
        self.run_thread()

    def connexion_sever(self):
        self.host = 'localhost'
        self.port = 12340
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect((self.host, self.port))
        self.notification('connecting to sensor sever')

    def notification(self, message):
        Notification(
            title=message,
            # description='Notification Description',
            icon_path='path/to/image/file/icon.png',
            duration=5,
            urgency='normal'
        ).send()

    def identification(self):
        self.username = socket.gethostname()
        self.client.send(self.username.encode('utf-8'))
        self.ip_address = self.client.getsockname()[0]

    def run_thread(self):
        self.receive_thread = Receive_thread(self.client, parent=None)
        self.receive_thread.state_signal.connect(self.con_not)
        self.receive_thread.message_signal.connect(self.weather)
        self.receive_thread.start()

    def con_not(self, state):
        data = state + ' ' + 'to the sensor server'
        self.statusbar.showMessage(data)

    def weather(self, message):
        time = dt.datetime.now()
        msg = message.split(' ')
        temperature = 'Temperature: ' + msg[0] + u'\u2103'
        humidity = 'Humidity: ' + msg[1] + '%'
        self.temp_pushButton.setText(temperature)
        self.hum_pushButton.setText(humidity)
        temp = float(msg[0])
        hum = float(msg[1])
        self.temp_array.append(temp)
        self.hum_array.append(hum)
        self.time_array.append(time)
        self.data
        self.temp_pushButton.clicked.connect(lambda: self.temp_window(self.temp_array, self.time_array))
        self.hum_pushButton.clicked.connect(lambda: self.hum_window(self.hum_array, self.time_array))
        self.close_window()

    def temp_window(self, temperature, time):
        self.temp_graph_window = TempGraph(temperature, time, parent=None)
        self.temp_graph_window.show()

    def hum_window(self, humidity, time):
        self.hum_graph_window = HumGraph(humidity, time, parent=None)
        self.hum_graph_window.show()

    def save_data(self, data):
        # Define the name of the file base on the date
        self.today = datetime.date.today()
        self.filename = str(self.today) + '.json'
        # create directory if does not exist
        self.fileDir = os.path.dirname(os.path.abspath(__file__))
        self.directory = self.fileDir + '/datas_files'
        if not os.path.isdir(self.directory):
            os.mkdir(self.directory)
        self.PATH = self.directory + '/' + self.filename
        # save the data in a file with the json extension
        with open(self.PATH, 'w') as file:
            json.dump(data, file, default=str)
        file.close()

    def close_window(self):
        try:
            self.quit = QtWidgets.QAction("Quit", self)
            self.save_data(self.data)
            self.quit.triggered.connect(self.close)
        except Exception as e:
            print(e)
            print('not saved')


def main():
    app = QApplication(sys.argv)
    form = WeatherApp()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
