# raspberry__sensor_temperature_humidity_server_client_Qtgui

A raspberry project :  

equipment used :  
- Rapsberry pi3 ModelB+
- Adafruit AM2320 Sensor

To install the sensor on the raspberry :  
https://learn.adafruit.com/circuitpython-on-raspberrypi-linux/installing-circuitpython-on-raspberry-pi



Receive the data temperature and humidity of a sensor plug to the raspberry on your laptop with a socket server/client.  
the files th_sensor.py and sensor_server on the raspberry.
And hum_graph, temp_graph, sensor_gui and sensor_client ont the laptop.

![Sample Video](images_videos/sensor_app.mp4 "Qt_sensor_app")


The data are saved automatically in a file with the day date format (year-month-day), example: 2021-05-19.json  
visualize the saved data with plot_data:  
![alt text](images_videos/plot_data.png "plot of the temperature and the humidity")  

Python librairies use for:
- the plot style: mplcyberpunk

- the notification: pynotifier  

## Usage
run on the raspberry the server:  
```bash
python sensor_server.py
```
  
run on the laptop:  
```bash
python sensor_client.py
```  

to plot the data:  
```bash
python plot_data.py -f [year-month-day.json]
```  
for example:
```bash
python plot_data.py -f 2021-05-19.json
```  
